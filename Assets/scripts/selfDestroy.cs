﻿using UnityEngine;
using System.Collections;

public class selfDestroy : MonoBehaviour {

	public GameObject dead;

	// Use this for initialization
	void Start () {


	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "bullet"){
            GameObject power = GameObject.Find("powerCore");
            if (power.GetComponent<Renderer>().material.color == GetComponentInChildren<Renderer>().material.color){
				Instantiate(dead, transform.position, transform.rotation);
				Destroy(gameObject);
				FireExtract.points++;
				FireExtract2.points++;
            }
        }
        else if(collision.gameObject.tag == "ship"){
            Destroy(gameObject);
        }
    }

}
