﻿using UnityEngine;
using System.Collections;

public class TriggerSelfDestroy : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    //This works like onCollision without applying physic
    // Perfect for bullet as we don't want to affect 
    // enemy trajectory with them
    void OnTriggerEnter(Collider collider)
    {
        Destroy(gameObject);
    }
}
