﻿using UnityEngine;
using System.Collections;

public class rotateSilender : MonoBehaviour {

	public float rotateSpeed = 10;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey ("right")) {
			transform.Rotate(Vector3.back * rotateSpeed * Time.deltaTime);			              
				}
		if (Input.GetKey ("left")) {
			transform.Rotate(Vector3.back * -rotateSpeed * Time.deltaTime);

				}
	
	}
}
