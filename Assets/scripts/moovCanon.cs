﻿using UnityEngine;
using System.Collections;

public class moovCanon : MonoBehaviour {

	public float rotateSpeed = 1;
	public float scrollDistance = 100;
	public float scrollSpeed = 10;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		RaycastHit hit;
		if (Physics.Raycast (ray, out hit)) {
			transform.LookAt(hit.point);
				}

		float rotateZ = Input.GetAxis ("Horizontal") * rotateSpeed;
		float rotateX = Input.GetAxis ("Vertical") * rotateSpeed;

		transform.Rotate (rotateX, 0, -rotateZ);
	}
}
