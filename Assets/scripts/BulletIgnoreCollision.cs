﻿using UnityEngine;
using System.Collections;

public class BulletIgnoreCollision : MonoBehaviour {

	// Use this for initialization
	void Start () {
        GameObject power = GameObject.Find("powerCore");
        GameObject canon2 = GameObject.Find("MiningLazer2");
        GameObject canon1 = GameObject.Find("MiningLazer");
        Physics.IgnoreCollision(this.GetComponent<Collider>(), power.GetComponent<Collider>());
        Physics.IgnoreCollision(this.GetComponent<Collider>(), canon2.GetComponent<Collider>());
        Physics.IgnoreCollision(this.GetComponent<Collider>(), canon1.GetComponent<Collider>());
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
