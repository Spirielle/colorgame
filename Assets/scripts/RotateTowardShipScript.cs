﻿using UnityEngine;
using System.Collections;

public class RotateTowardShipScript : MonoBehaviour {

    private Transform target;
    float turnSpeed = 5.0f;
    private Vector3 direction;

	// Use this for initialization
	void Start () {
	    target = GameObject.FindWithTag("target").transform;
	}
	
	// Update is called once per frame
	void Update () {
        if (target)
        {
            //direction = target.position - getComponent.transform.position;
            direction.Normalize();
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction), turnSpeed * Time.deltaTime);
        }
	
	}
}