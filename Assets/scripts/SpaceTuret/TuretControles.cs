﻿using UnityEngine;
using System.Collections;

public class TuretControles : MonoBehaviour {
	
	public float turetSpeed = 1;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey("a")){
			transform.RotateAround(transform.parent.position, Vector3.down, turetSpeed);
		}
		if (Input.GetKey("d")){
			transform.RotateAround(transform.parent.position, Vector3.up, turetSpeed);
		}
	}
}
