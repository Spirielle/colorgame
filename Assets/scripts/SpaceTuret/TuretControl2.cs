﻿using UnityEngine;
using System.Collections;

public class TuretControl2 : MonoBehaviour {

	public float turetSpeed = 1;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey("left")){
			transform.RotateAround(transform.parent.position, Vector3.down, turetSpeed);
		}
		if (Input.GetKey("right")){
			transform.RotateAround(transform.parent.position, Vector3.up, turetSpeed);
		}
	}
}