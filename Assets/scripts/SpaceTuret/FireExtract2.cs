﻿using UnityEngine;
using System.Collections;

public class FireExtract2 : MonoBehaviour {

		public GameObject Projectile;
		public float fireSpeed = 100;
		
		public AudioClip shoot;
		AudioSource audio;

		public static int points;
		
		// Use this for initialization
		void Start () {
		audio = GetComponent<AudioSource>();
		}
		
		// Update is called once per frame
		void Update () {
			
			if (Input.GetKeyUp("up")) {
			GameObject ProjectileInstance = Instantiate(Projectile, transform.position + transform.forward, transform.rotation) as GameObject;
            //Added * -1 to fire in the opposite direction
				ProjectileInstance.GetComponent<Rigidbody>().AddForce(transform.forward * -1 * fireSpeed);

				audio.PlayOneShot(shoot);
			}
		}


	void OnGUI () {
		GUI.Label (new Rect (10, 10, 100, 20), "Score: " + points);
		}
	}

